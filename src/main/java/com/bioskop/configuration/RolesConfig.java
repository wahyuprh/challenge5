package com.bioskop.configuration;


import com.bioskop.Enumeration.ERole;
import com.bioskop.model.Roles;
import com.bioskop.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RolesConfig {

    private static final Logger LOG = LoggerFactory.getLogger(RolesConfig.class);

    @Autowired
    RoleRepository roleRepository;

    @Bean
    public void prerun(){
        for(ERole c : ERole.values()){
            try {
                Roles roles = roleRepository.findByName(c)
                        .orElseThrow(()-> new RuntimeException("Roles not found"));
            } catch (RuntimeException rte){
                LOG.info("Role "+c.name()+"is not found, inserting to DB ...");
                Roles roles = new Roles();
                roles.setName(c);
                roleRepository.save(roles);
            }
        }
    }

}