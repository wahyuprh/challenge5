package com.bioskop.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public OpenAPI customOpenAPI(@Value("saat ini program tiket bioskop yang saya buat memiliki fungsi CRUD yang bertujuan untuk menambah, memperbarui, menghapus dan menampilkan informasi tentang pengguna dan film")String appDescription,
                                 @Value("Alpha Test")String appVersion){
        return new OpenAPI().info(
                new Info()
                        .title("Tiket Bioskop API")
                        .version(appVersion)
                        .description(appDescription)
                        .termsOfService("http://swager.io/terms")
                        .contact(new Contact()
                                .name("Wahyu prihantono")
                                .email("@wahyuprihantono29@gmail.com")
                                .url("https://gitlab.com/wahyuprh"))
                        .license(new License()
                                .name("Apache 2.1")
                                .url("http://springdocs.org"))
        );
    }
}
