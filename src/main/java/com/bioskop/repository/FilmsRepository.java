package com.bioskop.repository;

import com.bioskop.model.Films;
import com.bioskop.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmsRepository extends JpaRepository<Films, Integer> {

    public Films findByFilmId(Integer filmId);
    public Films findByFilmCode(String filmCode);
    public Films findByFilmName(String filmName);

    // Get Film tayang by isShow
    @Query(value = "select * from Films f where f.is_show=true", nativeQuery = true)
    List<Films> getFilmTayang();
}
