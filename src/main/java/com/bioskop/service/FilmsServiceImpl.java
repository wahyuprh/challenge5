package com.bioskop.service;

import com.bioskop.model.Films;
import com.bioskop.model.Schedules;
import com.bioskop.repository.FilmsRepository;
import com.bioskop.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService {

    @Autowired
    private FilmsRepository filmsRepository;

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Autowired
    public FilmsServiceImpl(FilmsRepository filmsRepository) {
        this.filmsRepository = filmsRepository;
    }


    @Override
    public void addSchedule(Integer filmId, String tanggalTayang, String jamMulai, String jamSelesai, String hargaTiket) {
        Schedules schedules = new Schedules();
        schedules.setTanggalTayang(tanggalTayang.toString());
        schedules.setJamMulai(jamMulai.toString());
        schedules.setJamSelesai(jamSelesai.toString());
        schedules.setHargaTiket(Integer.parseInt(hargaTiket.toString()));
        Films films = filmsRepository.findByFilmId(filmId);
        schedules.setFilmId(films);
        schedulesRepository.save(schedules);
    }

    @Override
    public Films getFilmByFilmName(String filmName) {
        return filmsRepository.findByFilmName(filmName);
    }

    @Override
    public Films getFilmByFilmId(Integer filmId) {
        return filmsRepository.findByFilmId(filmId);
    }

    @Override
    public Films addFilm(Films films) {
        films.getFilmCode();
        films.getFilmName();
        films.getIsShow();
        return filmsRepository.save(films);
    }

    @Override
    public Films updateFilm(Films films) {
        films.getFilmCode();
        Films updateFilm = filmsRepository.findByFilmId(films.getFilmId());
        updateFilm.setFilmCode(films.getFilmCode());
        updateFilm.setFilmName(films.getFilmName());
        updateFilm.setIsShow(films.getIsShow());
        return filmsRepository.save(films);
    }

    @Override
    public String deleteFilm(Integer filmId) {
        filmsRepository.deleteById(filmId);
        return "Delete " + filmId + " success!";
    }

    @Override
    public void getSchedulesFilms(Integer filmId) {
        List<Schedules> listSchedules = schedulesRepository.findSchedulesByFilmId(filmId);
        listSchedules.forEach(schedules -> System.out.println(listSchedules.toString()));
    }

    @Override
    public List<Films> getFilmTayang() {
        return filmsRepository.getFilmTayang();
    }

    @Override
    public List<Films> getAllFilms() {
        return filmsRepository.findAll();
    }
}