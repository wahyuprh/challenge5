package com.bioskop.service;

import com.bioskop.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersService {

    Users getUserByUsername(String username);

    Users getUserByEmail(String email);

    Users addUser(Users users);

    Users updateUserById(Users users);

    String deleteUser(Integer userId);

    List<Users> getAllUsers();
}