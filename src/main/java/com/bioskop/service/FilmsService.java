package com.bioskop.service;

import com.bioskop.model.Films;
import com.bioskop.model.Schedules;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmsService {

    void addSchedule(Integer filmId, String tanggalTayang, String jamMulai, String jamSelesai, String hargaTiket);

    Films getFilmByFilmName(String filmName);

    Films getFilmByFilmId(Integer filmId);

    Films addFilm(Films films);

    Films updateFilm(Films films);

    String deleteFilm(Integer filmId);

    void getSchedulesFilms(Integer filmId);

    List<Films> getFilmTayang();

    List<Films> getAllFilms();
}