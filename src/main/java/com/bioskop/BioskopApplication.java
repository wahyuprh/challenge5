package com.bioskop;

import com.bioskop.service.FilmsService;
import com.bioskop.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** to do list:
 * Implement film tayang & schedule */

@SpringBootApplication
public class BioskopApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(BioskopApplication.class, args);
    }

    @Autowired
    private FilmsService filmsService;

    @Override
    public void run(String... args) throws Exception {
    }
}
