package com.bioskop.model;

import com.bioskop.Enumeration.ERole;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    public Roles() {
    }

    public Roles(Integer roleId, ERole name){
        this.roleId = roleId;
        this.name = name;
    }
}