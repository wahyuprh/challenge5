package com.bioskop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@IdClass(SeatsId.class)
public class Seats implements Serializable {
    @Id
    private Character studioName;

    @Id
    private String seatsCode;
}
